libjira-client-perl (0.45-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 27 Nov 2022 14:46:28 +0000

libjira-client-perl (0.45-2) unstable; urgency=medium

  [ Laurent Baillet ]
  * fix lintian wrong-path-for-interpreter error

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 15 Jun 2022 14:55:09 +0100

libjira-client-perl (0.45-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Thu, 07 Jan 2021 13:24:02 +0100

libjira-client-perl (0.45-1) unstable; urgency=medium

  * Import upstream version 0.45
  * debian/patches/spelling-errors: Remove, applied upstream.
  * debian/libjira-client-perl.docs: Remove TODO from docs.

 -- Angel Abad <angel@debian.org>  Tue, 13 Dec 2016 09:38:10 +0100

libjira-client-perl (0.44-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.
  * Remove Jonathan Yu from Uploaders. Thanks for your work!

  [ Angel Abad ]
  * Team upload.
  * Import upstream version 0.44
  * debian/copyright: Update years.
  * Declare compliance with Debian Policy 3.9.8.
  * debian/patches/spelling-errors: Fix spelling errors.

 -- Angel Abad <angel@debian.org>  Sun, 11 Dec 2016 10:25:52 +0100

libjira-client-perl (0.43-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata.
  * Import upstream version 0.43.
  * Update years of upstream and packaging copyright.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.
  * Bump debhelper compatibility level to 9.

 -- gregor herrmann <gregoa@debian.org>  Sat, 05 Dec 2015 22:25:02 +0100

libjira-client-perl (0.42-1) unstable; urgency=medium

  * Imported Upstream version 0.42
  * debian/copyright: Update upstream years.

 -- Angel Abad <angel@debian.org>  Sun, 10 Aug 2014 10:37:51 +0200

libjira-client-perl (0.41-1) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Angel Abad ]
  * Imported Upstream version 0.41
  * debian/copyright: Update debian/* section
  * Bump Standards-Version to 3.9.5 (no changes)

 -- Angel Abad <angel@debian.org>  Sun, 20 Apr 2014 19:10:24 +0200

libjira-client-perl (0.40-1) unstable; urgency=low

  * Imported Upstream version 0.40

 -- Angel Abad <angel@debian.org>  Mon, 06 Aug 2012 12:42:23 +0200

libjira-client-perl (0.38-1) unstable; urgency=low

  * Imported Upstream version 0.38
  * debian/control: {Build-}Depends on liburi-perl

 -- Angel Abad <angel@debian.org>  Sun, 15 Jul 2012 17:08:00 +0200

libjira-client-perl (0.37-1) unstable; urgency=low

  * New upstream release.
  * Add (build) dependency on libdata-util-perl.
  * Update years of packaging copyright.
  * Add /me to Uploaders.

 -- gregor herrmann <gregoa@debian.org>  Sun, 27 May 2012 14:48:27 +0200

libjira-client-perl (0.36-1) unstable; urgency=low

  * Imported Upstream version 0.36
  * debian/patches/fix_spelling_error: Removed, applied upstream

 -- Angel Abad <angel@debian.org>  Sun, 22 Apr 2012 12:15:39 +0200

libjira-client-perl (0.35-1) unstable; urgency=low

  * Imported Upstream version 0.35
  * debian/copyright: Update format to copyright-format 1.0
  * Bump Standards-Version to 3.9.3
  * debian/patches/fix_spelling_error: Fix spelling error in manpage

 -- Angel Abad <angel@debian.org>  Wed, 18 Apr 2012 13:25:50 +0200

libjira-client-perl (0.34-1) unstable; urgency=low

  * Imported Upstream version 0.34

 -- Angel Abad <angel@debian.org>  Wed, 22 Feb 2012 12:44:21 +0100

libjira-client-perl (0.33-1) unstable; urgency=low

  * Email change: Angel Abad -> angel@debian.org
  * Imported Upstream version 0.33
  * debian/copyright: Update years

 -- Angel Abad <angel@debian.org>  Tue, 21 Feb 2012 11:05:46 +0100

libjira-client-perl (0.32-1) unstable; urgency=low

  * Imported Upstream version 0.32

 -- Angel Abad <angelabad@gmail.com>  Wed, 07 Dec 2011 14:22:47 +0100

libjira-client-perl (0.31-1) unstable; urgency=low

  * Team upload.
  * New upstream release.
  * Update years of packaging copyright.

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Nov 2011 17:20:37 +0100

libjira-client-perl (0.30-1) unstable; urgency=low

  * Imported Upstream version 0.30

 -- Angel Abad <angelabad@gmail.com>  Tue, 27 Sep 2011 07:38:04 +0200

libjira-client-perl (0.29-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ Angel Abad ]
  * Imported Upstream version 0.29
  * debian/copyright: Update debian/* section

 -- Angel Abad <angelabad@gmail.com>  Wed, 14 Sep 2011 12:25:05 +0200

libjira-client-perl (0.28-1) unstable; urgency=low

  * New upstream release
  * debian/copyright: Update copyright years
  * Bump Standards-Version to 3.9.2 (no changes)

 -- Angel Abad <angelabad@gmail.com>  Fri, 13 May 2011 16:54:58 +0200

libjira-client-perl (0.27-1) unstable; urgency=low

  * New upstream release
  * Bump to debhelper compat 8
  * Update copyright information
  * Disable author tests (we can remove deps that may cause FTBFS
    and also get rid of the no-perlcritic patch)

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 06 Mar 2011 13:13:09 -0500

libjira-client-perl (0.26-1) unstable; urgency=low

  [ Angel Abad ]
  * New upstream release

  [ gregor herrmann ]
  * debian/copyright: refresh license stanzas.

 -- Angel Abad <angelabad@gmail.com>  Fri, 24 Dec 2010 16:18:04 +0100

libjira-client-perl (0.25-1) unstable; urgency=low

  * New upstream release
  * Switch to dpkg-source format 3.0 (quilt)
    - Remove quilt framework
  * debian/copyirght: Update license information
  * Bump Standards-Version to 3.9.1 (no changes)

 -- Angel Abad <angelabad@gmail.com>  Mon, 13 Sep 2010 17:48:59 +0200

libjira-client-perl (0.24-1) unstable; urgency=low

  * New upstream release
  * Drop unnecessary version deps (satisfied by oldstable)

 -- Jonathan Yu <jawnsy@cpan.org>  Fri, 25 Dec 2009 06:38:19 -0500

libjira-client-perl (0.23-1) unstable; urgency=low

  * New upstream release

 -- Angel Abad <angelabad@gmail.com>  Sun, 13 Dec 2009 16:03:35 +0100

libjira-client-perl (0.22-1) unstable; urgency=low

  * New upstream release

 -- Angel Abad <angelabad@gmail.com>  Tue, 01 Dec 2009 21:42:54 +0100

libjira-client-perl (0.21-1) unstable; urgency=low

  [ Jonathan Yu ]
  * New upstream release
  * Install new examples from upstream

  [ gregor herrmann ]
  * Add patch to skip Perl::Critic tests; add quilt framework.

 -- Jonathan Yu <jawnsy@cpan.org>  Sun, 22 Nov 2009 15:11:08 -0500

libjira-client-perl (0.19-1) unstable; urgency=low

  * New upstream release
  * Add myself to Uploaders and Copyright

 -- Jonathan Yu <jawnsy@cpan.org>  Fri, 06 Nov 2009 18:37:54 -0500

libjira-client-perl (0.18-1) unstable; urgency=low

  * New upstream release
  * Add libtest-kwalitee-perl to B-D-I

 -- Angel Abad <angelabad@gmail.com>  Sun, 25 Oct 2009 08:59:01 +0100

libjira-client-perl (0.17-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: improve long description, thanks to Gerfried Fuchs for the
    bug report (closes: #551318).

  [ Angel Abad ]
  * Update my email address
  * New upstream release
  * debian/rules: Enable author tests
  * debian/control: Build-Depends on debhelper (>= 7.0.50) for overrides

 -- Angel Abad <angelabad@gmail.com>  Mon, 19 Oct 2009 11:41:29 +0200

libjira-client-perl (0.16-1) unstable; urgency=low

  * New upstream release

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Mon, 05 Oct 2009 13:23:48 +0200

libjira-client-perl (0.15-1) unstable; urgency=low

  * New upstream release

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Sun, 04 Oct 2009 15:36:53 +0200

libjira-client-perl (0.14-1) unstable; urgency=low

  * Initial Release. (Closes: #548162)

 -- Angel Abad (Ikusnet SLL) <angel@grupoikusnet.com>  Wed, 30 Sep 2009 07:45:15 +0200
